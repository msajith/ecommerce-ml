package com.ic.ecomm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.predictionio.sdk.java.Event;
import org.apache.predictionio.sdk.java.EventClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableList;
import com.ic.ecomm.domain.AppUser;
import com.ic.ecomm.domain.Product;
import com.ic.ecomm.repository.AppUserRepository;
import com.ic.ecomm.repository.ProductRepository;




@RestController
@RequestMapping("/api")
public class TrainingController {

	
	@Autowired
	private EventClient client;

	@Bean
	public EventClient getEventClient(@Value("${predictionio.accesskey}") String ACCESS_KEY,@Value("${predictionio.url}") String PREDICTIONIO_URL) {

		EventClient client = new EventClient(ACCESS_KEY,PREDICTIONIO_URL);
		System.out.println("ACCESS_KEY:  >> "+ACCESS_KEY);
		System.out.println("PREDICTIONIO_URL:  >> "+PREDICTIONIO_URL);
		
		return client;
	}


	private final Logger log = LoggerFactory.getLogger(TrainingController.class);
	//private final CubaUserRepository cubaUserRepository;
	private final ProductRepository productRepository;
	
	private final AppUserRepository appUserRepository;

	public TrainingController(AppUserRepository appUserRepository,ProductRepository productRepository) {
		this.appUserRepository = appUserRepository;
		this.productRepository = productRepository;
		

	}


	@PostMapping("/train")
	public String startTraing() throws URISyntaxException {
		log.debug("REST request to save Brand : {}", "");

		
		 List<AppUser> userList = createUserEvents();
		 
		
		 List<Product> productList = createItemEvents();
		 
		 
		 
		 createViewBuyEvents(userList, productList);

		return "";
	}


	private void createViewBuyEvents(List<AppUser> userList,  List<Product> productList) {
		int productCount = productList.size();
		for (AppUser appUser : userList) {
			 
			 for (int i = 0; i < 10; i++) {
				 
				 int randomIntView = ThreadLocalRandom.current().nextInt(1, productCount-1);
				 Event viewEvent = new Event()
						    .event("view")
						    .entityType("user")
						    .entityId(appUser.getUserName())
						    .targetEntityType("item")
						    .targetEntityId(String.valueOf(productList.get(randomIntView).getId()));
						try {
							client.createEvent(viewEvent);
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				
			}
			 
			 int randomIntBuy = ThreadLocalRandom.current().nextInt(1, productCount-1);
			 Event buyEvent = new Event()
					    .event("buy")
					    .entityType("user")
					    .entityId(appUser.getUserName())
					    .targetEntityType("item")
					    .targetEntityId(String.valueOf(productList.get(randomIntBuy).getId()));
					try {
						client.createEvent(buyEvent);
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			 
			 
			 
				
		 }
	}


	private List<Product> createItemEvents() {
		List<Product> productList =  productRepository.findAll();
		for (Product product : productList) {
			System.out.println(product.getId());
			if(product != null && product.getProductCategoryId() != null) {
				
				Event itemEvent = new Event()
						  .event("$set")
						  .entityType("item")
						  .entityId(String.valueOf(product.getId()))
						  .property("categories", ImmutableList.of(product.getProductCategoryId().getCategoryId()));
		
						try {
							client.createEvent(itemEvent);
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			}
			
		}
		return productList;
	}


	private List<AppUser> createUserEvents() {
		List<AppUser> userList = appUserRepository.findAll();
		for (AppUser appUser : userList) {
			//System.out.println(cubaUser);
			Event userEvent = new Event()
					.event("$set")
					.entityType("user")
					.entityId(appUser.getUserName());
			try {
				client.createEvent(userEvent);
			} catch (ExecutionException | InterruptedException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return userList;
	}


}
