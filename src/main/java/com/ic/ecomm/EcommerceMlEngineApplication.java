package com.ic.ecomm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.ic.ecomm"})
@EntityScan("com.ic.ecomm.domain")
@EnableJpaRepositories("com.ic.ecomm.repository")

public class EcommerceMlEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceMlEngineApplication.class, args);
	}

}
