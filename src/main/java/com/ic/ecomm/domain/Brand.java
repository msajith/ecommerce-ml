package com.ic.ecomm.domain;



import javax.persistence.*;

import java.io.Serializable;

/**
 * A Brand.
 */
@Entity
@Table(name = "brands")
public class Brand implements Serializable {

    private static final long serialVersionUID = 1L;

   
  
    @Id
    @Column(name = "brand_id")
    private String brandId;

    @Column(name = "brand_name")
    private String name;

 
    public String getBrandId() {
        return brandId;
    }

    public Brand brandId(String brandId) {
        this.brandId = brandId;
        return this;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public Brand name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Brand)) {
            return false;
        }
        return brandId != null && brandId.equals(((Brand) o).brandId);
    }

    @Override
    public int hashCode() {
        return 31;
    }

 
}
