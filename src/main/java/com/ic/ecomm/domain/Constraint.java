package com.ic.ecomm.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "SEC_CONSTRAINT")
public class Constraint {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "id")
	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
	private UUID id;

	
	@Column(name = "CHECK_TYPE", length = 50, nullable = false)
	protected String checkType;

	@Column(name = "OPERATION_TYPE", length = 50, nullable = false)
	protected String operationType;

	@Column(name = "CODE", length = 255)
	protected String code;

	@Column(name = "ENTITY_NAME", length = 255, nullable = false)
	protected String entityName;

	@Column(name = "JOIN_CLAUSE", length = 500)
	protected String joinClause;

	@Column(name = "WHERE_CLAUSE", length = 1000)
	protected String whereClause;

	@Lob
	@Column(name = "GROOVY_SCRIPT")
	protected String groovyScript;

	@Lob
	@Column(name = "FILTER_XML")
	protected String filterXml;

	@Column(name = "IS_ACTIVE")
	protected Boolean isActive = true;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_ID")
	protected CubaGroup group;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCheckType() {
		return checkType;
	}

	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getJoinClause() {
		return joinClause;
	}

	public void setJoinClause(String joinClause) {
		this.joinClause = joinClause;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	public String getGroovyScript() {
		return groovyScript;
	}

	public void setGroovyScript(String groovyScript) {
		this.groovyScript = groovyScript;
	}

	public String getFilterXml() {
		return filterXml;
	}

	public void setFilterXml(String filterXml) {
		this.filterXml = filterXml;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public CubaGroup getGroup() {
		return group;
	}

	public void setGroup(CubaGroup group) {
		this.group = group;
	}
	

}
