package com.ic.ecomm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "admin_key")
    private String adminKey;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "description")
    private String description;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "name")
    private String name;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "sales_quantity", precision = 21, scale = 2)
    private BigDecimal salesQuantity;

    @Column(name = "tag_code")
    private String tagCode;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "indentifier_id")
    private String indentifierId;

    @Column(name = "identifier_id")
    private String identifierId;

    @Column(name = "product_group_id")
    private String productGroupId;

   
   
    @ManyToOne
    @JoinColumn(name = "brand_id",referencedColumnName="brand_id")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Brand brand;
    
    @ManyToOne
    @JsonIgnoreProperties("products")
    @JoinColumn(name = "product_category_id",referencedColumnName="category_id")
    private ProductCategory productCategoryId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public Product productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getClientId() {
        return clientId;
    }

    public Product clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAdminKey() {
        return adminKey;
    }

    public Product adminKey(String adminKey) {
        this.adminKey = adminKey;
        return this;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = adminKey;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Product createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public Product description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModelCode() {
        return modelCode;
    }

    public Product modelCode(String modelCode) {
        this.modelCode = modelCode;
        return this;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public Product recordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public BigDecimal getSalesQuantity() {
        return salesQuantity;
    }

    public Product salesQuantity(BigDecimal salesQuantity) {
        this.salesQuantity = salesQuantity;
        return this;
    }

    public void setSalesQuantity(BigDecimal salesQuantity) {
        this.salesQuantity = salesQuantity;
    }

    public String getTagCode() {
        return tagCode;
    }

    public Product tagCode(String tagCode) {
        this.tagCode = tagCode;
        return this;
    }

    public void setTagCode(String tagCode) {
        this.tagCode = tagCode;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Product updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIndentifierId() {
        return indentifierId;
    }

    public Product indentifierId(String indentifierId) {
        this.indentifierId = indentifierId;
        return this;
    }

    public void setIndentifierId(String indentifierId) {
        this.indentifierId = indentifierId;
    }

    public String getIdentifierId() {
        return identifierId;
    }

    public Product identifierId(String identifierId) {
        this.identifierId = identifierId;
        return this;
    }

    public void setIdentifierId(String identifierId) {
        this.identifierId = identifierId;
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public Product productGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
        return this;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public ProductCategory getProductCategoryId() {
        return productCategoryId;
    }

    public Product productCategoryId(ProductCategory productCategory) {
        this.productCategoryId = productCategory;
        return this;
    }

    public void setProductCategoryId(ProductCategory productCategory) {
        this.productCategoryId = productCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", productId='" + getProductId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", adminKey='" + getAdminKey() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", description='" + getDescription() + "'" +
            ", modelCode='" + getModelCode() + "'" +
            ", name='" + getName() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", salesQuantity=" + getSalesQuantity() +
            ", tagCode='" + getTagCode() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", indentifierId='" + getIndentifierId() + "'" +
            ", identifierId='" + getIdentifierId() + "'" +
            ", productGroupId='" + getProductGroupId() + "'" +
            "}";
    }
}
