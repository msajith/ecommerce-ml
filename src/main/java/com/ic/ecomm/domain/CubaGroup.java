package com.ic.ecomm.domain;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "SEC_GROUP")
public class CubaGroup {
	
	@Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "id")
	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
	private UUID id;

	
	@Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID")
    private CubaGroup parent;

    @OneToMany(mappedBy = "group")
    @OrderBy("level")
    private List<CubaGroupHierarchy> hierarchyList;

    @OneToMany(mappedBy = "group")

    private Set<Constraint> constraints;

    @OneToMany(mappedBy = "group",fetch = FetchType.EAGER)
    private Set<CubaSessionAttribute> sessionAttributes;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CubaGroup getParent() {
		return parent;
	}

	public void setParent(CubaGroup parent) {
		this.parent = parent;
	}

	public List<CubaGroupHierarchy> getHierarchyList() {
		return hierarchyList;
	}

	public void setHierarchyList(List<CubaGroupHierarchy> hierarchyList) {
		this.hierarchyList = hierarchyList;
	}

	public Set<Constraint> getConstraints() {
		return constraints;
	}

	public void setConstraints(Set<Constraint> constraints) {
		this.constraints = constraints;
	}

	public Set<CubaSessionAttribute> getSessionAttributes() {
		return sessionAttributes;
	}

	public void setSessionAttributes(Set<CubaSessionAttribute> sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}


    

}
