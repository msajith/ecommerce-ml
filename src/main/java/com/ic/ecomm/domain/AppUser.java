package com.ic.ecomm.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A AppUser.
 */
@Entity
@Table(name = "authentication")
public class AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

  
 

    @Id
    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "identifier_id")
    private String identifierId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "email")
    private String email;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "is_block")
    private Boolean isBlocked;

    @Column(name = "is_customer")
    private Boolean isCustomer;

    @Column(name = "is_inactive")
    private Boolean isInactive;

    @Column(name = "is_vendor")
    private Boolean isVendor;

    @Column(name = "lastlogin")
    private Instant lastLogin;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "login_otp")
    private String loginOtp;

    @Column(name = "login_status")
    private Integer loginStatus;

    @Column(name = "login_count")
    private Integer loginCount;

    @Column(name = "otp_time_stamp")
    private Instant otpTimestamp;

    @Column(name = "password")
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "reset_token")
    private String resetToken;

    @Column(name = "time_stamp")
    private Instant timestamp;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "user_role")
    private String userRole;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "is_agent")
    private Boolean isAgent;

    @Column(name = "is_bank_customer")
    private Boolean isBankCustomer;

    @Column(name = "verified_account")
    private Boolean verifiedAccount;

    @Column(name = "last_login_country")
    private String lastLoginCountry;

    @Column(name = "last_login_device_id")
    private String lastLoginDeviceId;

    @Column(name = "last_login_ip_address")
    private String lastLoginIpAddress;

    @Column(name = "failure_login_attempts")
    private Integer failureLoginAttempts;

   

   

    public String getCustomerId() {
        return customerId;
    }

    public AppUser customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getClientId() {
        return clientId;
    }

    public AppUser clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getIdentifierId() {
        return identifierId;
    }

    public AppUser identifierId(String identifierId) {
        this.identifierId = identifierId;
        return this;
    }

    public void setIdentifierId(String identifierId) {
        this.identifierId = identifierId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public AppUser createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmail() {
        return email;
    }

    public AppUser email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public AppUser firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Boolean isIsBlocked() {
        return isBlocked;
    }

    public AppUser isBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
        return this;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Boolean isIsCustomer() {
        return isCustomer;
    }

    public AppUser isCustomer(Boolean isCustomer) {
        this.isCustomer = isCustomer;
        return this;
    }

    public void setIsCustomer(Boolean isCustomer) {
        this.isCustomer = isCustomer;
    }

    public Boolean isIsInactive() {
        return isInactive;
    }

    public AppUser isInactive(Boolean isInactive) {
        this.isInactive = isInactive;
        return this;
    }

    public void setIsInactive(Boolean isInactive) {
        this.isInactive = isInactive;
    }

    public Boolean isIsVendor() {
        return isVendor;
    }

    public AppUser isVendor(Boolean isVendor) {
        this.isVendor = isVendor;
        return this;
    }

    public void setIsVendor(Boolean isVendor) {
        this.isVendor = isVendor;
    }

    public Instant getLastLogin() {
        return lastLogin;
    }

    public AppUser lastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public void setLastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getLastName() {
        return lastName;
    }

    public AppUser lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginOtp() {
        return loginOtp;
    }

    public AppUser loginOtp(String loginOtp) {
        this.loginOtp = loginOtp;
        return this;
    }

    public void setLoginOtp(String loginOtp) {
        this.loginOtp = loginOtp;
    }

    public Integer getLoginStatus() {
        return loginStatus;
    }

    public AppUser loginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
        return this;
    }

    public void setLoginStatus(Integer loginStatus) {
        this.loginStatus = loginStatus;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public AppUser loginCount(Integer loginCount) {
        this.loginCount = loginCount;
        return this;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    public Instant getOtpTimestamp() {
        return otpTimestamp;
    }

    public AppUser otpTimestamp(Instant otpTimestamp) {
        this.otpTimestamp = otpTimestamp;
        return this;
    }

    public void setOtpTimestamp(Instant otpTimestamp) {
        this.otpTimestamp = otpTimestamp;
    }

    public String getPassword() {
        return password;
    }

    public AppUser password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public AppUser phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getResetToken() {
        return resetToken;
    }

    public AppUser resetToken(String resetToken) {
        this.resetToken = resetToken;
        return this;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public AppUser timestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public AppUser updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserRole() {
        return userRole;
    }

    public AppUser userRole(String userRole) {
        this.userRole = userRole;
        return this;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public AppUser userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean isIsAgent() {
        return isAgent;
    }

    public AppUser isAgent(Boolean isAgent) {
        this.isAgent = isAgent;
        return this;
    }

    public void setIsAgent(Boolean isAgent) {
        this.isAgent = isAgent;
    }

    public Boolean isIsBankCustomer() {
        return isBankCustomer;
    }

    public AppUser isBankCustomer(Boolean isBankCustomer) {
        this.isBankCustomer = isBankCustomer;
        return this;
    }

    public void setIsBankCustomer(Boolean isBankCustomer) {
        this.isBankCustomer = isBankCustomer;
    }

    public Boolean isVerifiedAccount() {
        return verifiedAccount;
    }

    public AppUser verifiedAccount(Boolean verifiedAccount) {
        this.verifiedAccount = verifiedAccount;
        return this;
    }

    public void setVerifiedAccount(Boolean verifiedAccount) {
        this.verifiedAccount = verifiedAccount;
    }

    public String getLastLoginCountry() {
        return lastLoginCountry;
    }

    public AppUser lastLoginCountry(String lastLoginCountry) {
        this.lastLoginCountry = lastLoginCountry;
        return this;
    }

    public void setLastLoginCountry(String lastLoginCountry) {
        this.lastLoginCountry = lastLoginCountry;
    }

    public String getLastLoginDeviceId() {
        return lastLoginDeviceId;
    }

    public AppUser lastLoginDeviceId(String lastLoginDeviceId) {
        this.lastLoginDeviceId = lastLoginDeviceId;
        return this;
    }

    public void setLastLoginDeviceId(String lastLoginDeviceId) {
        this.lastLoginDeviceId = lastLoginDeviceId;
    }

    public String getLastLoginIpAddress() {
        return lastLoginIpAddress;
    }

    public AppUser lastLoginIpAddress(String lastLoginIpAddress) {
        this.lastLoginIpAddress = lastLoginIpAddress;
        return this;
    }

    public void setLastLoginIpAddress(String lastLoginIpAddress) {
        this.lastLoginIpAddress = lastLoginIpAddress;
    }

    public Integer getFailureLoginAttempts() {
        return failureLoginAttempts;
    }

    public AppUser failureLoginAttempts(Integer failureLoginAttempts) {
        this.failureLoginAttempts = failureLoginAttempts;
        return this;
    }

    public void setFailureLoginAttempts(Integer failureLoginAttempts) {
        this.failureLoginAttempts = failureLoginAttempts;
    }

   
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

   

    @Override
    public int hashCode() {
        return 31;
    }

   
}
